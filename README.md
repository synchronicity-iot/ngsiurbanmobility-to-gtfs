# SynchroniCity - NGSI Urban Mobility to GTFS
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
# Table of Contents
1. [Description](#description)
2. [Application theme functional requirements](#functional-requirements)
3. [Interface and API](#interface-api)
4. [Service Architecture](service-architecture)
5. [Service Deployment](#service-deployment) 
6. [System Requirements](#system-requirements)
6. [Support](#support)
7. [License](#license)
--------------------


<a name="description"></a>
# 1. Description

This #AtomicService enables to convert NGSI Entities compliant to the SynchroniCity [Urban Mobility Data Models](https://github.com/Fiware/dataModels/tree/master/specs/UrbanMobility) to the native [GTFS feed](https://developers.google.com/transit/gtfs/reference) format. This NodeJS tool will consume input entities to be mapped by querying directly a NGSI [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/) (e.g. trips belonging to a specific route, issued by a specific transport agency), and then it will return the translation as GTFS feed of the related entities in a compressed archive. In particular the atomic service consumes the Urban Mobility entities, related to transit services issued by specific Transport Agencies, from an Orion Context Broker and generates the corresponding GTFS feed archive. In particular, it performs the inverse mapping done when defining the NGSI Urban Mobility from GTFS specification. 

In addition, the service will keep updated, in the Context Broker, a specific NGSI entity compliant to [GTFSTransitFeedFile](https://gitlab.com/synchronicity-iot/synchronicity-data-models/tree/master/UrbanMobility/GtfsTransitFeedFile). This entity provides metadata about the resulting GTFS Feed, such as the location (URL) where the ZIP file containing the feed is stored and available. 
This process realizes the connection between this service and the [GTFS Fetcher]() atomic service.
The tool is developed in [Node.js](https://nodejs.org) and can be started both as a command line converter tool and as a server, which in this case will provide also the [Urban Mobility to GTFS APIs](#api-reference), periodic update of the GTFSFeed and of the related GTFSFeedFile entity.


# 2. Application theme functional requirements


<a name="interface-api"></a>
# 3. Interface and API
The tool, when running in Server Mode, exposes the so called **Urban Mobility to GTFS APIs**. They are composed by two sets of RESTful services:
 -	**Urban Mobility Query API**
 -	**GTFS Translation API**

The full specification is available at the [Apiary](https://ngsiurbanmobilitytogtfsapi.docs.apiary.io/)


<a name="service-architecture"></a>
# 4. Service Architecture
The service relies on SynchroniCity platform, in particular on the Context Broker and its NGSI APIs.
As depicted by the following image, it is composed by the following components, and it will also provide a RESTful API that exposes the service APIs.

![alt text](img/architecture.png)

The service is composed by the followings components:
-	**Urban Mobility to GTFS API**: It is the entry point of the service and exposes the functionalities provided by the service, implementing both Urban Mobility Query API (UM Query API) and GTFS Translation API, in order to expose their functionalities, described in §2.8.3.1. It realizes the API services through a RESTful standard interface and is responsible of validating the incoming requests, forwarding to other service components and finally returning appropriate payloads as response.

-	**Urban Mobility Grabber**: it is the connection bridge between the service and the Context Broker through NGSI API.
    It is in charge of making the NGSI requests in order to retrieve the Urban Mobility Context entities, which will be used either as Query result towards the upper UM Query API component or as input in the GTFS Feeds mapping process.
-	**NGSI to GTFS Mapper**: it implements the logic to map each incoming NGSI entity to a GTFS Table member, namely the CSV file, which will be included in the final GTFS Feed archive.
  	Output GTFS Member with internal checking logic derived from [GTFS Specification](https://developers.google.com/transit/gtfs/reference/), regarding mandatory fields and their semantic correctness.

-	**Service Controller**: this is the core of the service and is responsible of following tasks:

  -	Forwarding requests among other components and performing specific logic for each incoming request. It will instruct the Urban Mobility Grabber component to retrieve, through remote NGSI requests, all the entities needed to perform the translation to the GTFS Feed.
    Then, the Controller will interact also with the NGSI to GTFS Mapper, in order to translate all the collected entities into GTFS files and finally in a GTFS archive.
  -	Storing translated GTFS Feeds and keeping them updated by monitoring, through the Urban Mobility Grabber, the original NGSI entities present in the Context Broker.
    The GTFS Feed archive can be internally stored or, as depicted in teh figure, sent to an external Remote Directory.
  -	(Under development) Creating and updating the specific GTFSTransitFeedFile entity in the Context Broker. This entity provides metadata about the resulting GTFS Feed (stored either in a local or remote Directory), such as the URL where the resulting ZIP file is available to download. As described previously, this process is critical to realize the connection between this service and the GTFS Fetcher service (Section §2.7).

----------------------------------

<a name="service-deployment"></a>
# 5. Service Deployment

The Tool can be deployed in different ways, depending on the running mode selected.

## 5.1 Via Docker

The Tool can be instantiated as a Docker container, running in Server Mode, from the Docker Image published in the [Docker Hub](https://hub.docker.com/r/synchronicityiot/ngsiurbanmobility-to-gtfs) page.

It can be deployed using the provided `docker-compose` file, by following the steps below:
- As the container will attach to an external docker network, this **MUST** be created before launching, with the following command:
```
sudo docker network create main
```

- Rename the `config.template.js` file to `config.js` and modify accordingly to the [Configuration](#configuration) section below.
  - In this case, the **`mode`** field MUST be `server`.

- Go to the repository root folder and type following command:
```
sudo docker-compose -f deployment/docker-compose.yml up -d
```
- The container will start, mounting the config.js file and running in the (default) 8080 port.

## 5.2 Native installation

### Prerequisites

The tool requires [NodeJS](https://nodejs.org/it/) version >= 10.15.2 to be installed.

Clone this repository in order to start the tool either as command line interface or in the Server Mode.
Go to the repository root folder and type following command:

```
npm install
```

After selecting the run mode and configuring the tool correctly (through conf file or cli arguments), start with:

```
node app
```

>If you have selected the Command Line mode append to the previous command the appropriate arguments (as described in  [**Configuration**](#configuration) section). 

<a name="installation"></a>
--------------------
## 5.3 Configuration
The global setup is defined in the `config.js` file
- Rename the `config.template.js` file to `config.js` and modify accordingly to the following options:
   - **`mode`**: The run mode, accepted values are: `cli` or `server`.
   - **`env`**: The execution environment. Accepted values are:
     - **`debug`**: the logs are written in the **``/logs``** folder. 
     - **`production`**: the logs are displayed in the console output.

  - **`logLevel`**: logging level for [WINSTON](https://www.npmjs.com/package/winston). Accepted values are:
    - **`error`, `warn`, `info`, `verbose`, `debug`, `silly`**
  - **`httpPort`**: If `server` mode was selected, the port where the tool will listen.
  - **`agencyId`**: The ID of the transportation agency that released the information in the Urban Mobility entities to be converted into GTFS. This Id can be retrieved by using the Urban Mobility Query API provided by this tool (getAgencies), or alternatively by inspecting the `operatedBy` property of [GTFSRoute](https://github.com/Fiware/dataModels/blob/master/specs/UrbanMobility/GtfsRoute/doc/spec.md) entities present in the target Context Broker.
  - **`orionUrl`**: The target Orion Context Broker where to read the Urban Mobility entities to be mapped, and (soon) where will be written the GTFSFeedFile entity. 
  - **`outFilePath`**: The absolute path (for now only locally to where the tool runs), of the output Zip File containing the mapped GTFS Feed.

Optionally change the followings (If not needed, keep the default values):

   - **`orionAuthHeaderName`**: Authorization Header name (e.g. **X-Auth-Token** or **Authorization**) for Orion requests.
   - **`orionAuthToken`**:  Authorization token name for Orion requests (e.g. **Bearer XXX**).
   - **`fiwareService`**: **Fiware-Service** header to be put in the Orion requesst.
   - **`fiwareServicePath`**: **Fiware-ServicePath** header to be put in the Orion requests.
   - **`enableProxy`**: (`true`|`false`). Enable HTTP requests through a Proxy.
   - **`proxy`**: Insert in the form `http://user:pwd@proxyHost:proxyPort`
    
> **`IMPORTANT`** The tool takes its default configuration from the `config.js` file, but following configurations will be overriden if the corresponding parameters are provided as Command Line arguments (depending on the running mode selected for the tool) :
 - **`--agencyId`**, **`--ag`**
 - **`--orionUrl`**, **`--u`**
 - **`--outFilePath`**, **`--f`**
 - **`--orionAuthHeaderName`**, **`--oa`**
 - **`--orionAuthToken`**, **`--ot`**
 - **`--fiwareService`**, **`--fs`**
 - **`--fiwareServicePath`**, **`--fsp`**
 
> **`IMPORTANT`** If the tool runs in **Server Mode**, the following parameters, if provided as **headers** in the HTTP Requests, will be overriden:
  - `FiwareService`
  - `FiwareServicePath`
  
Please see the [Api reference](#api-reference) section to find out all the parameters for the Server Mode. 

----------------
<a name="system-requirements"></a>
# 6. System Requirements
Minimum requirements to run this atomic service:

Ubuntu 14.04
CPU Architecture (x86_64)

----------------
<a name="support"></a>
# 7. Support

Any feedback on this documentation is highly welcome, including bugs, typos and suggestions.

The recommended way to provide feedback and receive support is to use repository [Issues](https://gitlab.com/synchronicity-iot/ngsiurbanmobility-to-gtfs/issues) and [Merge Requests](https://gitlab.com/synchronicity-iot/ngsiurbanmobility-to-gtfs/merge_requests).

<a name="license"></a>
# 8. License & Terms and Conditions

The NGSI Urban Mobility to GTFS service is licensed under Affero General Public License (GPL) version 3. [See here](https://www.gnu.org/licenses/agpl-3.0.html).

Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.