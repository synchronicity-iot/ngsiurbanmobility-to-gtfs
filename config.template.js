﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/



var config = {

    /********************** GLOBAL APPLICATION CONFIGURATION *****************
    * Followings are related to global configurations of the application
    **/
    env: 'debug', // debug or production
    mode: 'server', // cli or server 
    logLevel: 'debug', // error, warn, info, verbose, debug or silly
    httpPort: 8080, // PORT where the application will listen if ran in server mode

    /************************** Urban Mobility to GTFS CONFIGURATION **********
     * Followings are realted to configuration for grabbing Urban Mobility entities
     * and for mapping to GTFS feeds
     **/
    agencyId: '',
    //csvDelimiter: {
    //    field: ',',
    //    wrap: '"',
    //    eol: "\\n"
    //}

};

/*************** ORION Context Broker CONFIGURATION **********************/
config.orion = {

    orionUrl: 'http://localhost:1026', // The Context Broker endpoint (baseUrl) where to grab UM entities
    orionAuthHeaderName: '', // Authorization Header name (e.g. X-Auth-Token or Authorization) for Orion request // Leave blank if any
    orionAuthToken: '', // Authorization token name for Orion request (e.g. Bearer XXX) // Leave blank if any
    fiwareService: 'test', // Fiware-Service header to be put in the Orion request
    fiwareServicePath: '/UrbanMobility', // Fiware-ServicePath header to be put in the Orion request
    enableProxy: false, // Enable HTTP requests through a Proxy
    proxy: '', // Insert in the form http://user:pwd@proxyHost:proxyPort
    orionPageLimit: 500, // Limit parameters used in the request for pagination
    maxRetry: 2, // Max retry number per entity POST
    parallelRequests: 30 // DO NOT TOUCH - Internal configuration for concurrent request parallelization
};

/*************** File Writer CONFIGURATION **********************/
config.file = {
    outFilePath: './result.zip', // The filename will be appended with the AgencyId if the GTFS is relative to a specific AgencyId (e.g. result_agency1.zip)
    publicFilePath: 'http://example.com/result.zip' //Soon
};


module.exports = config;