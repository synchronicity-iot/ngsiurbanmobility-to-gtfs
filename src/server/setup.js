﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const log = require('../utils/logger').app(module);
const config = require('../../config');
const confUtils = require('../utils/confUtils');
const _ = require('lodash');
//const bodyParser = require('body-parser');
const express = require('express');
const busboy = require('connect-busboy');
const boolParser = require('express-query-boolean');

const translateAPI = require('./translateAPI');
const queryAPI = require('./queryAPI');

const errorHandler = require('api-error-handler');

const setAuthHeaders = (hdrs) => {
    if (_.has(hdrs, 'authorization')) {
        process.env.OAUTH_TOKEN = hdrs.authorization;
    }
    if (_.has(hdrs, 'x-auth-token')) {
        process.env.PAUTH_TOKEN = hdrs['x-auth-token'];
    }
};

const logErrors = (err, req, res, next) => {
    log.error(err.stack);
    next(err);
};

process.env.NODE_ENV = config.env;

module.exports = () => {

    /**************** General Express configuration ***************/

    const app = express();
    const port = config.httpPort || 8080;

    /************** Load Request Handler middlewares *************/

    log.info("Initializing Urban Mobility to GTFS Service in Server Mode");

    /** Initialize default configuration 
    *   Set default configuration from config.js file, these will be in case overriden by requests params
    **/
    if (confUtils.init()) {

        /******************* Middlewares configuration *******************/
        //app.use(bodyParser.urlencode({ extended: true }));
        //app.use(bodyParser.json());
        app.use(busboy()); // Insert the busboy middleware
        app.use(boolParser());
        app.use((req, res, next) => {
            setAuthHeaders(req.headers);
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
            next();
        });

        app.use('/api/urbanmobility', queryAPI);
        app.use('/api/gtfs', translateAPI);

        /******************** Error Handlers *****************************/
        // catch 404 and forward to error handler
        app.use(function (req, res, next) {
            var err = new Error('Not Found');
            err.status = 404;
            next(err);
        });
        app.use(errorHandler());

        /****************************************************************/
       
        /********* Start Server listening **************/
        app.listen(port, () => log.info(`Urban Mobility to GTFS Service listening on port ${port}!`));

    } else {
        log.error("There was an error while initializing Urban Mobility to GTFS Service configuration");
    }


};
