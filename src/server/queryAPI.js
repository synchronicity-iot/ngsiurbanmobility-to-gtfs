﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

'use strict';
const express = require('express');
var router = express.Router();
const utils = require('../utils/utils');
const log = require('../utils/logger').app(module);

const controller = require('../controller');
const stream = require('stream');

router.get('/agencies', async function (req, res, next) {

    try {
        var result = await controller.getAgencies();
        res.send(result);

    } catch (err) {
        next(err);
    }
});

router.get('/routes', async function (req, res, next) {

    try {
        var result = await controller.getRoutes();
        res.send(result);

    } catch (err) {
        next(err);
    }
});

router.get('/routes/:agencyId', async function (req, res, next) {
    try {
        if (req.params.agencyId) {
            var result = await controller.getRoutesByAgencyId(req.params.agencyId);
            res.send(result);
        } else
            return next();
    } catch (err) {
        next(err);
    }
});

//router.get('/trips', async function (req, res, next) {

//    try {
//        var result = await controller.getTrips();
//        res.send(result);

//    } catch (err) {
//        next(err);
//    }
//});

router.get('/trips/:routeId', async function (req, res, next) {
    try {
        if (req.params.routeId) {
            var result = await controller.getTripsByRouteId(req.params.routeId);
            res.send(result);
        } else
            return next();
    } catch (err) {
        next(err);
    }
});

module.exports = router;