﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

'use strict';
const express = require('express');
var router = express.Router();
const utils = require('../utils/utils');
const log = require('../utils/logger').app(module);

const controller = require('../controller');
const stream = require('stream');

router.get('/agencies/:agencyId', async function (req, res, next) {
    try {

        process.env.fiwareService = req.headers.fiwareService || process.env.fiwareService;
        process.env.fiwareServicePath = req.headers.fiwareServicePath || process.env.fiwareServicePath;
        if (req.params.agencyId) {
            var zipResult = await controller.mapAgencyToGTFS(req.params.agencyId, req.query.writeZip, req.query.returnZip, req.query.forceUpdate, req.query.createGTFSFeedFile);
            if (Buffer.isBuffer(zipResult.zip)) {

                var readStream = new stream.PassThrough();
                readStream.end(zipResult.zip);

                res.set('Content-disposition', 'attachment; filename=' + zipResult.filename);
                res.set('Content-Type', 'application/zip');
                readStream.pipe(res);

            } else if (req.query.returnZip) {
                log.debug("The result is not a Buffer!");
                next(new Error("The result is not a Buffer!"));
            } else {
                res.status(204).send({});
            }
        } else
            return next();
    } catch (err) {
        next(err);
    }
});

module.exports = router;