﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const pathParse = require('parse-filepath');
const ngsiDateFormat = /([12]\d{3})-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])/;

const parseFilePath = (pathString) => {
    return pathParse(pathString);
};

const ngsiToGtfsDate = (ngsiDate) => {

    var match = ngsiDate.match(ngsiDateFormat);
    if (match && match.length == 4)
        return `${match[1]}${match[2]}${match[3]}`;
    else
        throw new Error(`Illegal NGSI Date: ${ngsiDate}`);
};


module.exports = {
    parseFilePath: parseFilePath,
    ngsiToGtfsDate: ngsiToGtfsDate
};