﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const nconf = require('nconf');
const config = require('../../config');
const log = require('./logger').app(module);
const path = require('path');
const pathPattern = /^.+(\/|\\)[^\/|\\]+$/g;

nconf.use('memory');

process.argv.forEach(function (val, index, array) {
    nconf.argv({
        'agencyId': {
            alias: 'ag',
            describe: 'ID of transport Agency of Urban Mobility entities to be mapped into GTFS feeds',
            type: 'string',
            demand: false
        },
        'site': {
            alias: 'si',
            describe: 'Site part of SynchroniCity Entity Id pattern. It can represent a RZ, City or area that includes several different IoT deployments, services or apps (e.g., Porto, Milano, Santander, Aarhus, Andorra, etc)',
            type: 'string',
            demand: false
        },
        'service': {
            alias: 'se',
            describe: 'Service part of SynchroniCity Entity Id pattern. It can represent a represents a smart city service/application domain for example parking, garbage, environmental etc',
            type: 'string',
            demand: false
        },
        'group': {
            alias: 'gr',
            describe: 'Group part of SynchroniCity Entity Id pattern. IT can be used for grouping assets under the same service and/or provider (so it can be used to identify different IoT providers). It is responsibility of OS sites to maintain proper group keys',
            type: 'string',
            demand: false
        },
        'orionUrl': {
            alias: 'u',
            describe: 'URL of the context broker from where Urban Mobility entities will be read',
            type: 'string',
            demand: false
        },
        'orionAuthHeaderName': {
            alias: 'oa',
            describe: 'Authorization header name to be put in the Orion Request (with form "orionAuthHeaderName : orionAuthToken)"',
            type: 'string'
        },
        'orionAuthToken': {
            alias: 'ot',
            describe: 'Authorization Token to be put in the Orion Request (with form "orionAuthHeaderName : orionAuthToken)"',
            type: 'string'
        },
        'fiwareService': {
            alias: 'fs',
            describe: 'Fiware-Service header to be put in the Orion request',
            type: 'string'
        },
        'fiwareServicePath': {
            alias: 'fsp',
            describe: 'Fiware-ServicePath header to be put in the Orion request"',
            type: 'string'
        },
        'outFilePath': {
            alias: 'f',
            describe: 'Output ZIP file path where to printout mapped GTFS feeds',
            type: 'string',
            demand: false
        },
        'h': {
            alias: 'help',
            describe: 'Print the help message',
            demand: false
        }
    }).add('file', { type: 'literal', store: config });
});

const help = () => {
    if (nconf.get('h')) {
        nconf.stores.argv.showHelp();
        process.exit(0);
    }
};


/* Check if mandatory configuration parameters are set either via CLI args or config file 
 * 
 **/
const checkAndInitConf = () => {

    /************ CONFIGURATION PARAMETERS ************/
    var agencyId = nconf.get('agencyId') || config.agencyId;
    if (!agencyId && !config.mode === 'server') {
        log.error('You need to specify the Agency Id');
        return false;
    }


    //if (!nconf.get('site')) {
    //    log.error('You need to specify the site part of ID Pattern');
    //    return false;
    //}


    //if (!nconf.get('service')) {
    //    log.error('You need to specify the service part of ID Pattern');
    //    return false;
    //}


    //if (!nconf.get('group')) {
    //    log.error('You need to specify the group part of ID Pattern');
    //    return false;
    //}

    /*********************** ORION READER CONFIGURATION PARAMETERS *********/
    nconf.set('orionUrl', nconf.get('orionUrl') || config.orion.orionUrl);
    if (!nconf.get('orionUrl')) {
        log.error('You need to specify the remote URL of Orion Context Broker');
        return false;
    }

    nconf.set('orionPageLimit', nconf.get('orionPageLimit') || config.orion.orionPageLimit);
    if (!nconf.get('orionPageLimit')) {
        log.error('You need to specify the Page Limit of Orion Context Broker');
        return false;
    }

    nconf.set('fiwareService', nconf.get('fiwareService') || config.orion.fiwareService);
    //if (!nconf.get('fiwareService')) {
    //    log.error('You need to specify the Fiware-Service header of Orion Context Broker');
    //    return false;
    //}

    nconf.set('fiwareServicePath', nconf.get('fiwareServicePath') || config.orion.fiwareServicePath);
    //if (!nconf.get('fiwareServicePath')) {
    //    log.error('You need to specify the Fiware-ServicePath header of Orion Context Broker');
    //    return false;
    //}

    nconf.set('orionAuthHeaderName', nconf.get('orionAuthHeaderName') || config.orion.orionAuthHeaderName);
    //if (!nconf.get('orionAuthHeaderName')) {
    //    log.error('You need to specify the Authorization Header Name of Orion Context Broker');
    //    return false;
    //}

    nconf.set('orionAuthToken', nconf.get('orionAuthToken') || config.orion.orionAuthToken);
    if (nconf.get('orionAuthHeaderName') && !nconf.get('orionAuthToken')) {
        log.error('You need to specify the Authorization Token of Orion Context Broker');
        return false;
    }
    if (!nconf.get('orionAuthHeaderName') && nconf.get('orionAuthToken')) {
        log.error('You need also to set the Authorization Header Name parameter');
        return false;
    }

    /*********************** FILE WRITER CONFIGURATION PARAMETERS *********/
    nconf.set('outFilePath', nconf.get('outFilePath') || config.file.outFilePath);
    if (!nconf.get('outFilePath')) {
        log.error('You need to specify the ZIP file path where to printout mapped GTFS feeds');
        return false;
    }

    /******* Set initialized confs as Global variables ***************/
    global.process.env.agencyId = agencyId;
    global.process.env.orionUrl = nconf.get('orionUrl');
    global.process.env.updateMode = nconf.get('updateMode');
    global.process.env.fiwareService = nconf.get('fiwareService');
    global.process.env.fiwareServicePath = nconf.get('fiwareServicePath');
    global.process.env.orionAuthHeaderName = nconf.get('orionAuthHeaderName');
    global.process.env.orionAuthToken = nconf.get('orionAuthToken');
    global.process.env.orionPageLimit = nconf.get('orionPageLimit');
    global.process.env.outFilePath = nconf.get('outFilePath');

    return true;
};

const init = () => {
    help();
    return checkAndInitConf();
};

const getParam = (par) => {
    return nconf.get(par);
};



module.exports = {
    help: help,
    init: init,
    getParam: getParam
};