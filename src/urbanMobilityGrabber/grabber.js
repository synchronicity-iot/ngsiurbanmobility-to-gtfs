/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const orionReader = require('./orionConnector');

const buildRequestHeaders = () => {

    var headerObject = {
        'Fiware-Service': process.env.fiwareService,
        'Fiware-ServicePath': process.env.fiwareServicePath
    };

    if (process.env.orionAuthHeaderName && process.env.orionAuthToken)
        headerObject[process.env.orionAuthHeaderName] = process.env.orionAuthToken;

    return headerObject;

};

const getAgencies = async () => {
    return doPaginatedQuery(orionReader.getEntitiesByType, "GtfsAgency");
};

const getAgency = async (agencyId) => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsAgency", agencyId);
    if (result.length != 1)
        return Promise.reject(`No GtfsAgency with Id: ${agencyId} found!`);
    else
        return result[0];
};

const getRoutes = async () => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByType, "GtfsRoute");
    if (result.length == 0)
        return Promise.reject('No GtfsRoute found');
    else
        return result;
};

const getRoutesByAgencyId = async agencyId => {
    var result = doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsRoute",
        operatedBy: agencyId
    });
    if (result.length == 0)
        return Promise.reject(`No GtfsRoute found by AgencyId: ${agencyId}`);
    else
        return result;
};

const getRoute = async routeId => {
    var result = doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsRoute", routeId);
    if (result.length !== 1)
        return Promise.reject(`No GtfsRoute with Id: ${routeId} found!`);
    else
        return result[0];
};

const getServices = async () => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByType, "GtfsService");
    if (result.length == 0)
        return Promise.reject('No GtfsService found');
    else
        return result;
};

const getServicesByAgencyId = async agencyId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsService",
        operatedBy: agencyId
    });
    if (result.length == 0)
        return Promise.reject(`No GtfsService found by AgencyId: ${agencyId}`);
    else
        return result;
};

const getService = async serviceId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsService", serviceId);
    if (result.length !== 1)
        return Promise.reject(`No GtfsRoute with Id: ${serviceId} found!`);
    else
        return result[0];
};

const getTripsByRouteId = async routeId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsTrip",
        hasRoute: routeId
    });
    if (result.length == 0)
        return Promise.reject(`No GtfsTrip found by RouteId: ${routeId}`);
    else
        return result;

};

const getStopTimesByTripId = async tripId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsStopTime",
        hasTrip: tripId
    });
    if (result.length == 0)
        return Promise.reject(`No GtfsStopTime found by TripId: ${tripId}`);
    else
        return result;
};

const getFrequenciesByTripId = async tripId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsFrequency",
        hasTrip: tripId
    });
    return result;
};

const getShape = async shapeId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsShape", shapeId);
    if (result.length !== 1)
        return Promise.reject(`No GtfsShape with Id: ${serviceId} found!`);
    else
        return result[0];
};

const getCalendarDateRulesByServiceId = async serviceId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsCalendarDateRule",
        hasService: serviceId
    });

    return result;

};

const getCalendarRulesByServiceId = async serviceId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsCalendarRule",
        hasService: serviceId
    });
    if (result.length == 0)
        return Promise.reject(`No GtfsCalendarRule found by ServiceId: ${serviceId}`);
    else
        return result;
};

const getStop = async stopId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsStop", stopId);
    if (result.length !== 1)
        return Promise.reject(`No GtfsStop with Id: ${stopId} found!`);
    else
        return result[0];

};

const getStation = async stationId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByTypeAndId, "GtfsStation", stationId);
    if (result.length !== 1)
        return Promise.reject(`No GtfsStation with Id: ${stationId} found!`);
    else
        return result[0];
};

const getStationByStopId = async stopId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsStation",
        hasStop: stopId
    });
    return result[0];
};


const getAccessPointByStationId = async stationId => {
    var result = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsAccessPoint",
        hasParentStation: stationId
    });
    return result;
};

const getTransferRuleByStationOrStopId = async id => {

    var resultHasOrigin = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsTransferRule",
        hasOrigin: id
    });

    var resultHasDestination = await doPaginatedQuery(orionReader.getEntitiesByQuery, {
        type: "GtfsTransferRule",
        hasDestination: id
    });

    var result = [...resultHasOrigin, ...resultHasDestination];
    if (result.length == 0)
        return Promise.reject(`No GtfsTransferRule found by StationId or StopId: ${id}`);
    else
        return result;
};




/* Wrapper for handling paginated queries, (byQuery, byType...) based on returned count */
const doPaginatedQuery = async (fn, ...args) => {

    var collectedResults = [];
    var pageLimit = Number(process.env.orionPageLimit);

    /* First request to get the count and collect results from first page */
    var firstResultBody = undefined;

    switch (fn.name) {
        case 'getEntitiesByType': firstResultBody = await fn(args[0], 0, pageLimit, buildRequestHeaders());
            break;
        case 'getEntitiesByTypeAndId': firstResultBody = await fn(args[0], args[1], 0, pageLimit, buildRequestHeaders());
            break;
        default: firstResultBody = await fn(args[0], buildRequestHeaders());
    }

    for (let result of firstResultBody.results)
        collectedResults.push(result);

    var count = firstResultBody.count;
    if (count > pageLimit) {
        let pages = Math.floor(count / pageLimit);
        if (count % process.env.orionPageLimit > 0)
            pages++;

        let offset;
        if (typeof args[0] === 'object')
            args[0].offset = pageLimit;
        else
            offset = pageLimit;

        for (let i = 1; i <= pages; pages++) {

            let currentResultBody = undefined;
            switch (fn) {
                case 'getEntitiesByType':
                    currentResultBody = await fn(args[0], offset, pageLimit, buildRequestHeaders());
                    notQueryOffset += pageLimit;
                    break;
                case 'getEntitiesByTypeAndId':
                    currentResultBody = await fn(args[0], args[1], offset, pageLimit, buildRequestHeaders());
                    notQueryOffset += pageLimit;
                    break;
                default:
                    currentResultBody = await fn(args[0], buildRequestHeaders());
                    args[0].offset += pageLimit;
            }

            for (let result of currentResultBody.results)
                collectedResults.push(result);

        }
    }

    return collectedResults;
};


module.exports = {
    getAgencies: getAgencies,
    getAgency: getAgency,
    getRoutes: getRoutes,
    getRoutesByAgencyId: getRoutesByAgencyId,
    getRoute: getRoute,
    getServices: getServices,
    getServicesByAgencyId: getServicesByAgencyId,
    getService: getService,
    getTripsByRouteId: getTripsByRouteId,
    getStopTimesByTripId: getStopTimesByTripId,
    getFrequenciesByTripId: getFrequenciesByTripId,
    getShape: getShape,
    getCalendarDateRulesByServiceId: getCalendarDateRulesByServiceId,
    getCalendarRulesByServiceId: getCalendarRulesByServiceId,
    getStop: getStop,
    getStation: getStation,
    getAccessPointByStationId: getAccessPointByStationId,
    getTransferRuleByStationOrStopId: getTransferRuleByStationOrStopId,
    getStationByStopId: getStationByStopId
};