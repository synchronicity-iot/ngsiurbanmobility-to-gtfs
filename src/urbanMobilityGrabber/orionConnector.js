﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const rp = require('promise-request-retry');
const ngsi = require('ngsi-parser');
const config = require('../../config').orion;
const proxyConf = config.enableProxy ? config.proxy : undefined;
const log = require('../utils/logger').app(module);




/* ** GET /v2/entities?type=&options=count 
 *
 * Get entities count by type and idPattern
 */
const getEntitiesCountByType = async (type, idPattern, headers) => {

    return await getEntitiesRequest({
        type: type,
        options: "count",
        offset: 0,
        limit: 1
    }, headers);
};

/* ** GET /v2/entities?type=&offset=&limit=
 *
 * Get entities by type
 */
const getEntitiesByType = async (type, offset, limit, headers) => {

    return await getEntitiesRequest({
        type: type,
        options: "count,keyValues",
        offset: offset,
        limit: limit
    }, headers);
};

/* ** GET /v2/entities?type=&offset=&limit=
 *
 * Get entities by type
 */
const getEntitiesByTypeAndId = async (type, id, offset, limit, headers) => {

    return await getEntitiesRequest({
        type: type,
        id: id,
        options: "count,keyValues",
        offset: offset,
        limit: limit
    }, headers);
};

/* ** GET /v2/entities?id=
 *
 * Get entity by Id
 */
const getEntityById = async (id, offset, limit, headers) => {

    return await getEntitiesRequest({
        id: id,
        options: "count,keyValues",
        offset: offset,
        limit: limit
    }, headers);
};


/* ** GET /v2/entities?querystring
 *
 * Get entities by generic query
 */
const getEntitiesByQuery = async (query, headers) => {
    query.options = query.options ? `${query.options},count,keyValues` : `count,keyValues`;
    return await getEntitiesRequest(query, headers);
};



/* ** NGSI - GET /v2/entities */
const getEntitiesRequest = async (query, headers) => {

  
    // Convert query object to query string
    query.limit = query.limit | process.env.orionPageLimit;
    let queryString = ngsi.createQuery(query);

    log.debug(`Getting NGSI entities - q: ${queryString} `);

    var reqOptions = {
        method: 'GET',
        headers: headers,
        uri: process.env.orionUrl + '/v2/entities' + queryString,
        simple: false,
        json: true,
        resolveWithFullResponse: true,
        retry: config.maxRetry,
        proxy: proxyConf,
        rejectUnauthorized: false
    };

    try {

        var response = await rp(reqOptions);

        if (response.statusCode === 200) {

            log.debug('Query response correctly received');
            let respBody = response.body;
            if (respBody && Array.isArray(respBody))
                return Promise.resolve({
                    count: response.headers['fiware-total-count'],
                    results: response.body
                });
            //else
            //    return Promise.reject('Returned payload are not array or is empty');

        } else {
            log.error(`There was an error while querying entities: ${response.statusCode}: ${response.body}`);
            return Promise.reject(new Error(response.body.description));
        }

    } catch (error) {
        log.error(`There was an error while querying entities: ${error}`);
        return Promise.reject(error);
    }

};

const writeEntity = (objectToWrite) => {



};

module.exports = {
    getEntitiesCountByType: getEntitiesCountByType,
    getEntitiesByType: getEntitiesByType,
    getEntitiesByTypeAndId: getEntitiesByTypeAndId,
    getEntityById: getEntityById,
    getEntitiesByQuery: getEntitiesByQuery
};

