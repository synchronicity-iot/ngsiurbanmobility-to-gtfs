﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const grabber = require('./urbanMobilityGrabber/grabber');
const mapper = require('./gtfsMapper/mapper');

const log = require('./utils/logger').app(module);
const zipManager = require('./gtfsMapper/zipManager');
const utils = require('./utils/utils');

const mapAgencyToGTFS = async (agencyId, writeZip, returnZip = true, forceUpdate = false, createGTFSFeedFile = false) => {


    var grabbedEntities = [];
    var zipResult = zipManager.getGTFSZipFile(agencyId);

    if (!zipResult || !zipResult.zip || writeZip || forceUpdate) {

        // Grab all UM entities for agencyId
        log.info("Grabbing all the NGSI Urban Mobility entities from configured Context Broker");
        try {
            grabbedEntities = await getAllUMEntitiesByAgency(agencyId);
        } catch (error) {
            log.error(error);
            throw new Error(error);
        }
        // Map to CSVs and optionally WriteZip to filesystem and/or return to the caller
        log.info("Mapping to GTFS all grabbed NGSI Urban Mobility entities");

        writeZip = true;
        forceUpdate = true;
        try {
            zipResult.zip = await mapper.mapUMtoGTFS(grabbedEntities, writeZip, returnZip);
        } catch (error) {
            log.error(error);
            throw new Error(error);
        }
        //TODO: Create GTFSFeedFile and send to Orion
        if (createGTFSFeedFile)
            console.log("Creating GTFSFeedFile in the ContextBroker");

    }

    return zipResult;
};

const getAgencies = async () => {

    return await grabber.getAgencies();
};

const getRoutes = async () => {

    return await grabber.getRoutes();
};

const getRoutesByAgencyId = async (agencyId) => {

    return await grabber.getRoutesByAgencyId(agencyId);
};

const getTrips = async () => {

    return await grabber.getTrips();
};

const getTripsByRouteId = async (routeId) => {

    return await grabber.getTripsByRouteId(routeId);
};

const getAllUMEntitiesByAgency = async (agencyId) => {

    var totalCalendarDateRules = [];
    var totalCalendarRules = [];
    var totalTrips = [];
    var totalShapes = []
    var totalFrequencies = [];
    var totalStopTimes = []
    var totalStopsMap = new Map();
    var totalAccessPointsMap = new Map();
    var totalStationsMap = new Map();
    var totalTransferRulesMap = new Map();

    // Get Agency

    log.info("Retrieving GtfsAgency");
    var agency = await grabber.getAgency(agencyId);

    // Get all the Routes by Agency Id (operatedBy)
    var routes = await grabber.getRoutesByAgencyId(agencyId);

    // Get all the Services by Agency Id (operatedBy)
    var services = await grabber.getServicesByAgencyId(agencyId);

    // For each Service: get CalendarDateRules, get CalendarRules
    for (service of services) {

        let serviceId = service.id;
        let calendarDateRules = await grabber.getCalendarDateRulesByServiceId(serviceId);
        cleanCalendarDateRules(calendarDateRules);
        totalCalendarDateRules.push(...calendarDateRules);

        let calendarRules = await grabber.getCalendarRulesByServiceId(serviceId);
        cleanCalendarRules(calendarRules);
        totalCalendarRules.push(...calendarRules);
    }

    // For each Route: get all Trips by Route Id (hasRoute)
    for (let route of routes) {

        let routeId = route.id;
        let trips = await grabber.getTripsByRouteId(routeId);

        // For each Trip: get all StopTimes (hasTrip) , get all Frequencies (hasTrip), get Shape by id from Trip's hasShape,
        // get Service by id from Trips's hasService
        for (let trip of trips) {

            let tripId = trip.id;
            let stopTimes = await grabber.getStopTimesByTripId(tripId);

            // Get Frequency
            let frequencies = await grabber.getFrequenciesByTripId(tripId);
            totalFrequencies.push(...frequencies);

            // Get Shape from hasShape
            let shape = undefined;
            if (trip.hasShape)
                shape = await grabber.getShape(trip.hasShape);
            if (shape)
                totalShapes.push(shape);


            // For each StopTimes: get all Stops (0)
            for (let stopTime of stopTimes) {

                let hasStop = stopTime.hasStop;
                let stop = await grabber.getStop(hasStop);
                let stopId = stop.id;

                // For each Stop: get all Stations (1) (hasParentStation), 
                let hasParentStation = await stop.hasParentStation;

                // If Stop has hasParentStation get directly the Station by Id
                let station = undefined;
                if (hasParentStation) {

                    station = await grabber.getStation(hasParentStation);

                    // Check if retrieved Station has this Stop within its hasStop array

                    let stationHasStop = station.hasStop;
                    if (stationHasStop && stationHasStop.length > 0) {

                        if (!stationHasStop.find(x => x === stopId))
                            log.error(`The Station ${station.id} from Stop: ${stopId} , hasn't the Stop in its hasStop array!`);

                    } else {
                        log.error(`The Station ${station.id}  hasn't the mandatory hasStop array!`);
                    }

                } else {
                    // Lookup Station by hasStop == stopId
                    station = await grabber.getStationByStopId(stopId);
                }

                // If error, push Station and Stop anyway? 

                // Get all Transfers (with a Stop or Station as hasOrigin or hasDestination)
                try {

                    let transfers = await grabber.getTransferRuleByStationOrStopId(stopId)
                    for (transfer of transfers)
                        totalTransferRulesMap.set(transfer.transferType + transfer.hasOrigin + transfer.hasDestination, transfer);


                } catch (error) {
                    log.error(error);
                }

                // Push current Stop in the Map in order to avoid duplicates
                totalStopsMap.set(stop.id, stop);

                // Optionally, if there is a Station, get Transfers and AccessPoints
                if (station) {
                    let stationId = station.id;

                    // Get Transfers by StationId
                    try {
                        let transfers = await grabber.getTransferRuleByStationOrStopId(stationId);
                        for (transfer of transfers)
                            totalTransferRulesMap.set(transfer.transferType + transfer.hasOrigin + transfer.hasDestination, transfer);

                    } catch (error) {
                        log.error(error);
                    }

                    // Get Access Points by StationId
                    let accessPoints = await grabber.getAccessPointByStationId(stationId);

                    for (ap of accessPoints)
                        totalAccessPointsMap.set(ap.id, ap);

                    // Push station in the Map in order to avoid duplicates
                    totalStationsMap.set(stationId, station);

                }

                // Push StopTime
                totalStopTimes.push(stopTime);
            }

            // Check if the service of Trip's hasService is present and has the same agencyId (operatedBy) of the Road which Trip belongs to (hasRoute).
            let hasService = trip.hasService;

            // If error, pop the Service from global Services???
            if (hasService) {
                let service = services.find(x => x.id === hasService)

                if (!service)
                    log.error(`The Service: ${hasService} referred by hasService of Trip: ${tripId} is not present`);

                else if (!service.operatedBy)
                    log.error(`The Service has no valid operatedBy property`);
                else if (service.operatedBy !== route.operatedBy)
                    log.error(`The Service has an operatedBy property mismatching the one of Trip's Route`);
            }

            // Push Trip anyway ?
            totalTrips.push(trip);

        }

    }

    return {
        agency: agency,
        routes: routes,
        services: services,
        calendar: totalCalendarRules,
        calendar_dates: totalCalendarDateRules,
        trips: totalTrips,
        shapes: totalShapes,
        frequencies: totalFrequencies,
        stop_times: totalStopTimes,
        transfers: [...totalTransferRulesMap.values()],
        accessPoints: [...totalAccessPointsMap.values()],
        stations: [...totalStationsMap.values()],
        stops: [...totalStopsMap.values()],
    }

    // hasOrigin e hasDestination di TransferRule devono puntare ad una Station o Stop appartenenti allo stesso Trip.
};


const cleanCalendarDateRules = (calendarDateRules) => {
    calendarDateRules.map(calendarDate => {

        if (calendarDate['appliesOn'])
            try {
                calendarDate['appliesOn'] = utils.ngsiToGtfsDate(calendarDate['appliesOn']);
            } catch (error) {
                log.error(error);
            }
        return calendarDate;

    });
};

const cleanCalendarRules = (calendarRules) => {
    calendarRules.map(calendarRule => {

        if (calendarRule['startDate'])
            try {
                calendarRule['startDate'] = utils.ngsiToGtfsDate(calendarRule['startDate']);
            } catch (error) {
                log.error(error);
            }

        if (calendarRule['endDate'])
            try {
                calendarRule['endDate'] = utils.ngsiToGtfsDate(calendarRule['endDate']);
            } catch (error) {
                log.error(error);
            }

        return calendarRule;

    });
}

module.exports = {
    mapAgencyToGTFS: mapAgencyToGTFS,
    getAgencies: getAgencies,
    getRoutes: getRoutes,
    getRoutesByAgencyId: getRoutesByAgencyId,
    getTrips: getTrips,
    getTripsByRouteId: getTripsByRouteId
};