﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const commandLine = require('../utils/confUtils');
const controller = require('../controller');
const config = require('../../config');

const log = require('../utils/logger').app(module);

module.exports = async (agencyIdIn) => {
    log.info("Initializing Urban Mobility to GTFS Service in Command Line Mode");

    if (commandLine.init()) {

        var agencyId = agencyIdIn || commandLine.getParam('agencyId');
        try {

            await controller.mapAgencyToGTFS(agencyId,true,false);

        } catch (error) {
            log.error(error);
        }

    } else {
        log.error("There was an error while initializing Urban Mobility to GTFS Service configuration");
    }
};