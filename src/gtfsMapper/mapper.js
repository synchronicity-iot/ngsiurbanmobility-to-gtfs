/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const log = require('../utils/logger').app(module);
const configUtil = require('../utils/confUtils');
const converter = require('json-2-csv');
const jsonMapper = require('./intermediateMapper');
const zipManager = require('./zipManager');


const gtfsMaps = {
    agency: require('./jsonMaps/GtfsAgency'),
    routes: require('./jsonMaps/GtfsRoute'),
    trips: require('./jsonMaps/GtfsTrip'),
    calendar: require('./jsonMaps/GtfsCalendarRule'),
    calendar_dates: require('./jsonMaps/GtfsCalendarDateRule'),
    shapes: require('./jsonMaps/GtfsShape'),
    frequencies: require('./jsonMaps/GtfsFrequency'),
    stop_times: require('./jsonMaps/GtfsStopTime'),
    stops: require('./jsonMaps/GtfsStop'),
    transfers: require('./jsonMaps/GtfsTransferRule')
};

const ngsiToCSV = (array, keys) => {

    var options = {
        checkSchemaDifferences: false,
        //delimiter: configUtil.getParam('csvDelimiter'),
        emptyFieldValue: '',
        expandArrayObjects: true,
        keys: keys
    };

    return converter.json2csvAsync(array, options);
};


const mapUMtoGTFS = async (umObj, writeZip, returnZip) => {

    /* For each component of UMobj, map GTFS fields to intermediate JSON, convert it to CSV and add the file to the ZIP
    *
    * */

    var zip = undefined;
    // Services must not be mapped
    delete umObj['services'];

    for (let [key, value] of Object.entries(umObj)) {

        switch (key) {

            case 'services':
                // Handle services

                break;
            case 'accessPoints':
                for (let obj of value) {
                    obj.locationType = 2;
                    try {
                        ({ long: obj.stopLong, lat: obj.stopLat } = extractLocationToCoords(obj.location));
                    } catch (error) {
                        log.error(`Impossible to extract coordinates from ${obj.id} location, error: ${error}`);
                    }
                }
                break;
            case 'stations':
                for (let obj of value) {
                    obj.locationType = 1;
                    try {
                        ({ long: obj.stopLong, lat: obj.stopLat } = extractLocationToCoords(obj.location));
                    } catch (error) {
                        log.error(`Impossible to extract coordinates from ${obj.id} location, error: ${error}`);
                    }
                }
                break;
            case 'stops':
                for (let obj of value) {
                    obj.locationType = 0;
                    try {
                        ({ long: obj.stopLong, lat: obj.stopLat } = extractLocationToCoords(obj.location));
                    } catch (error) {
                        log.error(`Impossible to extract coordinates from ${obj.id} location, error: ${error}`);
                    }
                }
                value.push(...[...umObj.accessPoints, ...umObj.stations]);
                break;
            case 'shapes':
                try {
                    value = mapShapes(value);
                } catch (error) {
                    log.error(`Impossible to extract and transform Shapes from ${obd.id}, error: ${error}`);
                }
                break;
            default:

                break;
        }


        if (key !== 'accessPoints' && key !== 'stations') {
            try {
                let mappedJson = jsonMapper.mapJsonToJson(value, gtfsMaps[key]);
                if (!mappedJson || (Array.isArray(mappedJson) && mappedJson.length == 0)) {
                    // skip;
                    continue;

                } else {
                    let csv = await ngsiToCSV(mappedJson, Object.keys(gtfsMaps[key]));
                    if (csv.length > 1) {
                        log.debug(`Adding CSV entry: ${key}.txt into the ZIP`);
                        zip = zipManager.addCSVToZip(csv, `${key}.txt`, zip);
                    }
                }

            } catch (error) {
                log.error(error);
            }
        }

    }



    /***** Write and/or return retrieved or created GTFS Zip file ****/
    if (writeZip) {
        log.info("Writing mapped CSVs to Zip File");
        try {
            zipManager.writeZipToFile(zip, umObj.agency.id);
            log.info("GTFS Zip file correctly written");
        } catch (error) {
            log.error(error);
        }
    }

    if (returnZip) {
        log.info("Returning mapped CSVs as Zip Buffer")
        try {
            let buffer = zipManager.getZipBuffer(zip, umObj.agency.id);
            return buffer;
        } catch (error) {
            log.error(error);
        }
    }

};


const extractLocationToCoords = (location) => {
    var coordinates;
    if (location && (coordinates = location.coordinates) && Array.isArray(coordinates) && coordinates.length == 2)
        return {
            long: coordinates[0],
            lat: coordinates[1]
        };
    else
        throw new Error();
};

const mapShapes = (shapes) => {

    let resShapes = [];

    for (let shape of shapes) {
        let location = shape.location;
        let distanceTravelled = shape.distanceTravelled;

        let coordinates = undefined;
        if (location && (coordinates = location.coordinates) && Array.isArray(coordinates) && coordinates.length > 0) {

            let type = location.type;

            if (distanceTravelled && Array.isArray(distanceTravelled) && distanceTravelled.length > 0 && !coordinates.length === distanceTravelled.length) {
                throw new Error('The number of points in location.coordinates mismatches the number of distanceTravelled values');
            }


            if (type) {
                if (type === 'LineString') {
                    // Extract LineString points and match with distanceTravelled values

                    for (let [index, point] of coordinates.entries()) {

                        if (Array.isArray(point) && point.length === 2) {
                            resShapes.push({
                                id: shape.id,
                                shapePtLon: point[0],
                                shapePtLat: point[1],
                                shapePtSequence: index,
                                distanceTravelled: distanceTravelled && Array.isArray(distanceTravelled) ? distanceTravelled[index] : ''
                            });
                        } else {
                            throw new Error(`The Point: ${index} is not well formed`);
                        }
                    }

                } else if (type === 'MultiLineString') {
                    // Extract MultiLineString points and match with distanceTravelled values

                    for (let [lineIndex, line] of coordinates.entries()) {
                        if (Array.isArray(line)) {

                            for (let [index, point] of line.entries()) {
                                if (Array.isArray(point) && point.length === 2) {
                                    resShapes.push({
                                        id: shape.id,
                                        shapePtLon: point[0],
                                        shapePtLat: point[1],
                                        shapePtSequence: index,
                                        distanceTravelled: distanceTravelled && Array.isArray(distanceTravelled) ? distanceTravelled[index] : ''
                                    });
                                } else
                                    throw new Error(`The Point: ${index} is not well formed`);
                            }

                        } else
                            throw new Error(`The Line: ${lineIndex} is not well formed`);

                    }

                } else
                    throw new Error(`The type ${type} is not accepted`);

            } else
                throw new Error("Location type is missing");


        } else {
            throw new Error('Location is missing, empty or without coordinates');
        }
    }
    return resShapes;

}

module.exports = {
    mapUMtoGTFS: mapUMtoGTFS
}



