﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

const Zipper = require('adm-zip');

const fs = require('fs');
const configUtil = require('../utils/confUtils');
const utils = require('../utils/utils');
const filenameFilter = /(\\|\/|:|\*|\?|"|<|>|\|)/g;
const path = require('path');


const addCSVToZip = (csvText, entryName, zip) => {
    if (!zip)
        zip = new Zipper();

    zip.addFile(entryName, Buffer.from(csvText));
    return zip;

};

const writeZipToFile = (zip, agencyId) => {


    if (zip) {
        zip.writeZip(getGTFSZipFilename(agencyId));
    }
};

const getZipBuffer = (zip, agencyId) => {
    if (zip) {
        return zip.toBuffer();
    } else
        throw new Error();
};

const existsZip = (agencyId) => {

    return fs.existsSync(getGTFSZipFilename(agencyId));
};

const getGTFSZipFile = (agencyId) => {
    try {
        var filename = getGTFSZipFilename(agencyId);
        return {
            zip: fs.readFileSync(filename),
            filename: filename
        };
    } catch (error) {
        return {
            zip: undefined,
            filename: filename
        };;
    }
};


const getGTFSZipFilename = (agencyId) => {

    // Replace forbidden chars for filenames in the AgencyId
    var filteredAgencyId = agencyId ? `_${agencyId.replace(filenameFilter, "_")}` : '';
    var parsedConfPath = utils.parseFilePath(configUtil.getParam('outFilePath'));

    var confFilename = parsedConfPath.name;
    if (!confFilename)
        throw new Error('No Filename specified in the Configuration!');
    confFilename = `${confFilename}${filteredAgencyId}`;

    var extension = parsedConfPath.ext ? parsedConfPath.ext : '.zip';

    return path.join(parsedConfPath.dir, `${confFilename}${extension}`);
}

module.exports = {
    addCSVToZip: addCSVToZip,
    writeZipToFile: writeZipToFile,
    getZipBuffer: getZipBuffer,
    existsZip: existsZip,
    getGTFSZipFile: getGTFSZipFile
};