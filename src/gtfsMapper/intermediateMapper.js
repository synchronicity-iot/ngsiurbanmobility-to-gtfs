﻿/*******************************************************************************
 * NGSI Urban Mobility to GTFS
 *  Copyright (C) 2019 Engineering Ingegneria Informatica S.p.A.
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


var JM = require('json-mapper');
var log = require('../utils/logger');


const mapJsonToJson = (input, map) => {

    var converter = JM.makeConverter(map);
    var result = undefined;

    if (Array.isArray(input)) {
        result = [];
        for (let obj of input) {
            result.push(converter(obj));
        }
    } else {
        result = converter(input);
    }

    return result;

};


module.exports = {
    mapJsonToJson: mapJsonToJson

};