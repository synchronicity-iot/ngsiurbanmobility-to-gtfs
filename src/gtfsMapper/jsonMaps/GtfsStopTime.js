module.exports = {
    trip_id: 'hasTrip',
    stop_id: 'hasStop',
    arrival_time: 'arrivalTime',
    departure_time: 'departureTime',
    stop_sequence: 'stopSequence',
    stop_headsign: 'stopHeadsign',
    pickup_type: 'pickupType',
    drop_off_type: 'dropOffType',
    shape_dist_traveled: 'distanceTravelled',
    timepoint: 'timepoint'
};