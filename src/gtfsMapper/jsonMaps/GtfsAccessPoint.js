module.exports = {
    stop_id: 'id',
    parent_station: 'hasParentStation',
    stop_name: 'name',
    stop_code: 'code',
    stop_url: 'page',
    stop_desc: 'description',
    stop_long: 'stopLong',
    stop_lat: 'stopLat',
    zone_id: 'zoneCode',
    wheelchair_boarding: 'wheelChairAccessible',
    location_type: 'locationType'
};