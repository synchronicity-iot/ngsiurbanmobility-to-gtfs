module.exports = {
    agency_id: 'id',
	agency_name: 'name',
	agency_url: 'page',
	agency_timezone: 'timezone',
	agency_phone: 'phone',
	agency_lang: 'language'
};