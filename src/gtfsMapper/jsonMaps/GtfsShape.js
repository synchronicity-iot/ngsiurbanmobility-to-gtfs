module.exports = {
    shape_id: 'id',
    shape_pt_lat: 'shapePtLat',
    shape_pt_lon: 'shapePtLon',
    shape_pt_sequence: 'shapePtSequence',
    shape_dist_traveled: 'distanceTravelled'
};
