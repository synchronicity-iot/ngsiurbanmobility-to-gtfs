module.exports = {
    route_id: 'hasRoute',
    service_id: 'hasService',
    trip_id: 'id',
    shape_id: 'hasShape',
    trip_headsign: 'headSign',
    trip_short_name: 'shortName',
    direction_id: 'direction',
    block_id: 'block',
    wheelchair_accessible: 'wheelChairAccessible',
    bikes_allowed: 'bikesAllowed'
};
