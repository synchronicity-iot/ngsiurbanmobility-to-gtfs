module.exports = {
    transfer_type: 'transferType',
    min_tranfer_time: 'minimumTransferTime',
    from_stop_id: 'hasOrigin',
    to_stop_id: 'hasDestination'
};