module.exports = {
    trip_id: 'hasTrip',
    start_time: 'startTime',
    end_time: 'endTime',
    headway_secs: 'headwaySeconds',
    exact_times: 'exactTimes'
};