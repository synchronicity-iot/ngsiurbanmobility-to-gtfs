module.exports = {
    route_id: 'id', 
    agency_id: 'operatedBy',
	route_short_name: 'shortName',
	route_long_name: 'name',
	route_desc: 'description',
	route_url: 'page',
    route_color: 'routeColor',
    route_text_color: 'routeTextColor',
    route_sort_order: 'routeSortOrder',
    route_type: 'routeType'
};
