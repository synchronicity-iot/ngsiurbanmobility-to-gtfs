module.exports = {
    service_id: 'hasService',
    monday: 'monday',
    tuesday: 'tuesday',
    wednesday:'wednesday',
    thursday: 'thursday',
    friday: 'friday',
    saturday: 'saturday',
    sunday: 'sunday',
    start_date: 'startDate',
    end_date: 'endDate'
};